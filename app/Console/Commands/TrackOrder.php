<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

class TrackOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check order status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            logger('START:: Track order');
            $user = User::all();
            if( count($user) > 0){
                foreach ( $user as $key=>$val ){
                    $db_orders = Order::where('user_id', $val->id)->where('success_status', 0)->get();
                    if( count($db_orders) > 0){
                        foreach ( $db_orders as $dbokey=>$dboval ){
                            $curr_time = (strtotime(date('Y-m-d H:i:s')));
                            $create_date = (strtotime($dboval->order_created_at));
                            $interval  = abs($curr_time - $create_date);
                            $minutes   = round($interval / 60);
                            logger('Minutes:: '. $minutes);
                                $order_id = $dboval->order_id;
                                $query = $this->order($order_id);
                                $result = $this->request($query, $val->id);
                                if( !$result['errors'] ){
                                    $sh_order = (@$result['body']->container['data']['order']) ? $result['body']->container['data']['order'] : [];
                                    logger( $sh_order['legacyResourceId'] . ' :: ' .$sh_order['displayFulfillmentStatus'] );
                                    if( !empty($sh_order)){
                                        if( $minutes > 30 && $sh_order['displayFulfillmentStatus'] == 'UNFULFILLED' ){
                                            $this->sendMail($sh_order, $val);
                                            $dboval->success_status = 1;
                                            $dboval->save();
                                        }elseif (  $minutes > 30 && $sh_order['displayFulfillmentStatus'] == 'FULFILLED' ){
                                            $dboval->success_status = 1;
                                            $dboval->save();
                                        }
                                    }
                                }
                            }
                    }
                }
            }

        }catch( \Exception $e ){
            logger('ERROR:: Track order');
            logger(json_encode($e));
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function order($id)
    {
        try {
            return '{
                          order(id: "gid://shopify/Order/'.$id.'") {
                            displayFulfillmentStatus
                             legacyResourceId
                                name
                          }
                        }';
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param $query
     * @param $id
     * @return array|\GuzzleHttp\Promise\Promise
     * @throws \Exception
     */
    public function request($query, $id)
    {
        $shop = User::where('id', $id)->first();
        $parameter = [];
        $options = new Options();
        $options->setVersion(env('SHOPIFY_API_VERSION'));
        $api = new BasicShopifyAPI($options);
        $api->setSession(new Session(
            $shop->name, $shop->password));
        return $api->graph($query, $parameter);
    }

    public function sendMail($sh_order, $user){
        $query = '{
                          shop {
                            email
                          }
                    }';
        $result = $this->request($query, $user->id);
        $sh_shop = (@$result['body']->container['data']['shop']) ? $result['body']->container['data']['shop'] : [];
        if( !empty($sh_shop) ){
            $data = [
                'order_name' => $sh_order['name'],
                'order_link' => 'https://' . $user->name . '/admin/orders/' . $sh_order['legacyResourceId'],
            ];

            $email = $sh_shop['email'];
            $email = 'ruchita.crawlapps@gmail.com';
            Mail::send('TrackOrder.mail', $data, function ($message) use ($email) {
                $message->to($email, 'Order Tracking');
                $message->subject("Unfulfilled Tracking");
            });
        }
    }
}
