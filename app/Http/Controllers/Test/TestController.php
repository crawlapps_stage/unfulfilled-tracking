<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

class TestController extends Controller
{
    public function index(){
        try{
            $order_id = '2806678651040';
            $query = $this->order($order_id);
            $result = $this->request($query);
            dd($result);
        }catch ( \Exception $e ){
            dd($e);
        }
    }
    public function order($id)
    {
        try {
            return '{
                          order(id: "gid://shopify/Order/'.$id.'") {
                            displayFulfillmentStatus
                             legacyResourceId
                                name
                          }
                        }';
        } catch (\Exception $e) {
            dd($e);
        }
    }
    public function request($query)
    {
        $shop = \Auth::user();
        $parameter = [];
        $options = new Options();
        $options->setVersion(env('SHOPIFY_API_VERSION'));
        $api = new BasicShopifyAPI($options);
        $api->setSession(new Session(
            $shop->name, $shop->password));
        return $api->graph($query, $parameter);
    }
}
