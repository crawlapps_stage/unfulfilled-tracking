<?php namespace App\Jobs;

use App\Events\CheckOrder;
use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param ShopDomain $shopDomain The shop's myshopify domain
     * @param stdClass   $data       The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            \Log::info( '=============== START:: Order Create Webhook ==============');
            $shop_domain = $this->shopDomain->toNative();
            $shop = User::where('name', $shop_domain)->first();
            $sh_order = $this->data;

            $order = Order::where('order_id', $sh_order->id)->where('user_id', $shop->id)->first();

            if( !$order ){
                $order = new Order;
                $order->user_id = $shop->id;
                $order->order_id = $this->data->id;
                $order->success_status = 0;
                $order->fulfillment_status = $sh_order->fulfillment_status;
                $order->order_created_at = date("Y-m-d H:i:s", strtotime($sh_order->created_at));
                $order->save();
            }

            return response()->json(['data' => 'success'], 200);
        }catch( \Exception $e ){
            \Log::info( '=============== ERROR:: Order Create Webhook ==============');
            \Log::info( json_encode($e) );
        }

    }
}
